# Omnitest - Server 

Written in Typescript with NodeJS

## API Documentation
### User
#### SignUp `/auth/sign-up`
- Method: `POST`
- Parameter: **(json)**
  - `first_name`: `string`
  - `last_name`: `string`
  - `email`: `string`
  - `password`: `string`
#### Login `/auth/log-in`
- Method: `POST`
- Parameter: **(json)**
  - `email`: `string`
  - `password`: `string`

#### Send token to user email to verify account `/auth/send_verify_email`
- Method: `POST`
- Parameter: **(json)**
  - `email`: `string`

#### Verify account with token obtained from email(/auth/send_verify_email) `/auth/verify`
- Method: `POST`
- Parameter: **(json)**
  - `email`: `string`
  - `token`: `int`

#### Send token to user email to verify resetting password `/auth/send_verify_reset`
- Method: `POST`
- Parameter: **(json)**
  - `email`: `string`

#### Verify resetting password with token obtained from email(/auth/send_verify_reset) `/auth/reset`
- Method: `POST`
- Parameter: **(json)**
  - `email`: `string`
  - `password`: `string` (new password)
  - `token`: `int`

#### Change Password `/auth/changePassword`
- Method: `POST`
- Parameter: **(json)**
  - `email`: `string`
  - `old_password`: `string`
  - `new_password`: `string`

#### Load info User `/auth/infoUser`
- Method: `GET`
- Parameter: **(url)**
  - `id`: `int`
  >Sample: http://localhost:8080/auth/infoUser?id=1
#### Get list info User `/auth/getAllUserExceptMe`
- Method: `GET`
- Parameter: **(url)**
  - `id`: `int` (id of teacher)
  >Sample: http://localhost:8080/auth/getAllUserExceptMe?id=1

#### Get list tests that you have bookmarked `/auth/load_tests_bookmark`
- Method: `GET`
- Parameter: **(url)**
  - `id`: `int` (id user)
  >Sample: http://localhost:8080/auth/load_tests_bookmark?id=2

#### Bookmark the test `/auth/bookmark_test`
- Method: `POST`
- Parameter: **(json)**
  - `id_user`: `int` (id of user)
  - `id_test`: `int`

#### Delete bookmarking the test `/auth/delete_bookmark_test`
- Method: `POST`
- Parameter: **(json)**
  - `id_user`: `int` (id of user)
  - `id_tests`: `Array<int>`

#### Change information in profile for User `/auth/change_profile`
- Method: `POST`
- Parameter: **(json)**
  - `id`: `int` (id of user)
  - `first_name`: `string`
  - `last_name`: `string`
  - `phone_number`: `string`
  - `avatar`:`string|null`

### Class
#### Create Class `/class/create`
- Method: `POST`
- Parameter: **(json)**
  - `name`: `string` (name of class)
  - `id`: `int`

#### Rename Class `/class/rename`
- Method: `POST`
- Parameter: **(json)**
  - `id`: `int` (id of class)
  - `name_class`: `string`

#### Load list class of user(teacher) `/class/owned`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (idUser)
  >Sample: http://localhost:8080/class/owned?id=10

#### Load list class not of user(student) `/class/not-in`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (idUser)
  >Sample: http://localhost:8080/class/not-in?id=1

#### Delete Class `/class/delete`
- Method: `POST`
- Parameter: **(json)**
  - `id`: `int`

#### Request to Class `/class/request`
- Method: `POST`
- Parameter: **(json)**
  - `id`: `int` (id User)
  - `class`: `int` (id Class)

#### Permit Student to Class `/class/permit`
- Method: `POST`
- Parameter: **(json)**
  - `id`: `int` (id User)
  - `class`: `int` (id Class)

#### Load List Student of Class `/class/user`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (id User)
  >Sample: http://localhost:8080/class/user?id=1

#### Kick student out of class `/class/kick-out`
- Method: `POST`
- Parameter: **(json)**
  - `id_student`: `int`
  - `id_class`: `int`

#### Load List Student request to your classes `/class/user-requested`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (id Teacher (you!))
  >Sample: http://localhost:8080/class/user-requested?id=2

#### Load list Class, you are allowed to join `/class/pending-accept'
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (id User (or Student))
  >Sample: http://localhost:8080/class/pending-accept?id=2

#### Load list of classes you have joined `/class/your`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (id User)
  >Sample: http://localhost:8080/class/your?id=2

#### Load list tests in class `/class/test`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (idClass)
  >Sample: http://localhost:8080/class/test?id=3

#### Load list tests not in class `/class/test_not_in`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (idClass)
  >Sample: http://localhost:8080/class/test_not_in?id=3

#### Modify tests for class `/class/modify_test`
- Method: `POST`
- Parameter: **(json)**
  - `id_class`:`int`
  - `list_id_test`: `Array[int]`
  - `option` : `boolean` (true: insert, false: delete)

#### Load list of classes you have request into `/class/request_into_classes`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (id User (Student))
  >Sample: http://localhost:8080/class/request_into_classes?id=5

#### Load list of student is permitted into your class `/class/permitted_users`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (id User (Teacher))
  >Sample: http://localhost:8080/class/permitted_users?id=2

#### Cancel request into class `/class/cancel_request`
- Method: `POST`
- Parameter: **(json)**
  - `id_student`: `int`
  - `id_class`: `int`

#### Cancel student is permitted into class `/class/cancel_permit`
- Method: `POST`
- Parameter: **(json)**
  - `id_student`: `int`
  - `id_class`: `int`

#### Load detail of class `/class/detail`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (id Class)
  >Sample: http://localhost:8080/class/detail?id=3

### Homepage
#### Load Hot Test `/home/test_hot`
- Method: `GET`
- Parameter: **url**
  - `page`: `int` (>=1)
  <!-- - `id`: `int` (id of User) -->
  >Sample: http://localhost:8080/home/test_hot?page=1
  >Note: `number_of_do_times` is number of times students are doing the test! `PermitDo`: Allows students to do the test or not! (1: allow, 0: not allow) 
#### Load Hot Topic `/home/topic_hot`
- Method: `GET`
- Parameter: **url**
  - `page`: `int` (>=1)
  >Sample: http://localhost:8080/home/topic_hot?page=1
#### Load Test follow idTopic and sort by time `/home/loadTestFollowTopic` (only Public Test)
- Method: `GET`
- Parameter: **url**
  - `page`: `int` (>=1)
  - `id`: `int` (id of topic)
  - `order`: `boolean` (If it is true, then arrange from old to new, false is the opposite)
  - `key`: `string` (optional)
  >Sample: http://localhost:8080/home/loadTestFollowTopic?page=1&order=true&id=2&key=ABC (search) </br>
  >Sample: http://localhost:8080/home/loadTestFollowTopic?page=1&order=true&id=2&key= (loadAll) </br>
#### Load all Public Test or Search Public Test follow title `/home/search_public_test` (order by star desc)
- Method: `GET`
- Parameter: **url**
  - `key`: `string` (optional)
  - `page`: `int` (>=1)
  >Sample: http://localhost:8080/home/search_public_test?key=ABC&page=1 (search follow name)

  >Sample: http://localhost:8080/home/search_public_test?key=&page=1 (load all Public Test)
#### Load all Topic or Search Topic follow title `/home/search_topic` (order by view <!-- , only Topic contain Public Test -->)
- Method: `GET`
- Parameter: **url**
  - `key`: `string` (optional)
  - `page`: `int` (>=1)
  >Sample: http://localhost:8080/home/search_topic?key=ABC&page=1 (search follow name)</br>
  >Sample: http://localhost:8080/home/search_topic?key=&page=1 (load all Topic)
<!-- #### Load amount of notify about class `/home/number_notify`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (idUser)
  >Sample: http://localhost:8080/home/number_notify?id=3 -->
### Notify
#### Load amount of notify for user `/notify/number_notify`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (idUser)
  >Sample: http://localhost:8080/home/number_notify?id=3

#### Load list of notify for user `/notify/load_notify`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (idUser)
  - `page`: `int` (>=1)
  >Sample: http://localhost:8080/notify/load_notify?id=3&page=1 </br>
  >Note: for `seen`, 0: not seen, 1: seen

#### Mark notify has seen for user `/notify/mark_seen`
- Method: `POST`
- Parameter: **(json)**
  - `id`: `int` (idNotify)

### Image
#### Upload Image `/uploadImage`
- Method: `POST`
- Parameter: **(enctype="multipart/form-data")**
  - `image`: **File** (**File types are accepted**: _jpeg,jpg,png,gif_)
> **Note:** This api is used to upload images of Questions or answers!
> **Important:** To use it, You need to set the name of **\<input\>** tag is **'image'**. 
  > **Example for web:** ```<input type="file" name="image" class="input-group input-file"/>``` **_(Same for other types: app android, ios, react,...)_**

> **Note:** You need copy file _quickstart-1568790258503-firebase-adminsdk-9tylc-9a09ecb901.json_ to folder node_modules after build to use this api!!!
### Question
#### Create Question `/question/create`
- Method: `POST`
- Parameter: **(json)**
  - `id_teacher`: `int`
  - `type`: `string` ("choose", "match", "arrange", "fill")
  - `title`: `string` (with fill question, please add `[-]`, ... in the title)
  - `image_title`: `string` (image url, optional)
  - `answer`:  `[jsonAnswer]`
    - **jsonAnswer:** {
      - `text`: `string`, (split by "--")
      - `image`: `string` (image url, optional)
    }

  Sample: 
  ```javascript
  answer: [
    //With choose
    {
      "text":"True answer--1",
      "image":"https://ung-dung.com/images/upanh_online/upanh_lay_link_nhanh_7.jpg"
    },
    {
      "text":"False--0",
      "image":"https://img2.thuthuatphanmem.vn/uploads/2019/05/24/dung-link-anh-truc-tuyen_090357800.png"
    },
    {
      "text":"False again--0",
      "image":""
    }
    //With match
    {
      "text":"1+1?--2",
      "image":"https://firebasestorage.googleapis.com/v0/b/quickstart-1568790258503.appspot.com/o/Image_1588863788453?alt=media&token=1588863788453"
    }
    {
      "text":"How many wings does one duck have?--4",
      "image":""
    }
    //With arrange, right order
    {
      "text":"First sentence",
      "image":"https://firebasestorage.googleapis.com/v0/b/quickstart-1568790258503.appspot.com/o/Image_1588863788453?alt=media&token=1588863788453"
    }
    {
      "text":"Second sentence",
      "image":"https://firebasestorage.googleapis.com/v0/b/quickstart-1568790258503.appspot.com/o/Image_1588863788453?alt=media&token=1588863788453"
    }
    //With fill, valid order of [-],...
    {
      "text":"me",
      "image":""
    }
    {
      "text":"lazy",
      "image":"https://photo-2-baomoi.zadn.vn/w1000_r1/2019_04_12_310_30327481/8ac3afec46adaff3f6bc.jpg"
    }
  ]
  ```
  - `id_topic`: `int`

#### Update Question `/question/update`
- Method: `POST`
- Parameter: **(json)**
  - `id_question`: `int`
  - `id_teacher`: `int`
  - `type`: `string` ("choose", "match", "arrange", "fill")
  - `title`: `string` (with fill question, please add `[-]`, ... in the title)
  - `image_title`: `string` (image url, optional)
  - `id_topic`: `int`
  - `answer`:  `[jsonAnswer]` (similar to /question/create but is optional)
    - **jsonAnswer:** {
      - `text`: `string`, (split by "--")
      - `image`: `string` (image url, optional)
    }
  > **Note:** When you update type of question, you have to update answer! 
  > <br/>**Important:** Only have option update Answer when really needed. Otherwise will cause data redundancy!
  Sample:
  - Without update Answer:
  ```javascript
  {
    "id_question": 16,
    "type": "choose",
    "id_teacher": 1,
    "title": "CC",
    "image_title": "https://google.com",
    "id_topic": null
  }
  ```
  - With update Answer:
  ```javascript
  {
    "id_question": 16,
    "type": "choose",
    "id_teacher": 1,
    "title": "CC",
    "image_title": "https://google.com",
    "id_topic": null,
    "answer": [
        {
            "image": null,
            "text": "123--1"
        },
        {
            "image": null,
            "text": "123--0"
        },
        {
            "image": null,
            "text": "123--0"
        }
    ]
  }
  ```
#### Delete Question `/question/delete`
- Method: `POST`
- Parameter: **(json)**
  - `id_question`: `int` (!= 0)

#### Add the Tests or config info in the Tests for Question `/question/add_update_into_test`
- Method: `POST`
- Parameters: **(json)**
  - `id_question`: `int`
  - `id_tests`: `Array<infoQuestionOfTest>`
- Structure `infoQuestionOfTest`:
  - `id_test`: `int`
  - `time_limit`: `string`
  - `point`: `int`

#### Delete a Question from the Tests  `/question/delete_from_test`
- Method: `POST`
- Parameters: **(json)**
  - `id_question`: `int`
  - `list_id_test`: `Array<int>`

#### Load Question follow id(contains detailed answers) `/question/id`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (id of question!)
  >Sample: http://localhost:8080/question/id?id=1

#### Load info Question for update(contains detailed answers) `/question/loadForUpdate`
- Method: `GET`
- Parameter: **url**
  - `id_question`: `int` 
  - `id_teacher`: `int` 
  >Sample: http://localhost:8080/question/loadForUpdate?id_question=3&id_teacher=1

#### Load Question of User `/question/user`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (id of user who create question!)
  >Sample: http://localhost:8080/question/user?id=1

#### Load List Test of User contain the question `/question/in_tests`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int` (id of user who create question!)
  - `id_question`: `int`
  >Sample: http://localhost:8080/question/in_tests?id_question=2&id_user=1 <br/>
  Note: returned value such as point and time_limit is of question in test

#### Load List Test of User not contain the question `/question/not_in_tests`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int` (id of user who create question!)
  - `id_question`: `int`
  >Sample: http://localhost:8080/question/not_in_tests?id_question=2&id_user=1

#### Load List Questions which you shared for other `/question/list_question_you_shared`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int` (id of user who create question!)
  >Sample: http://localhost:8080/question/list_question_you_shared?id_user=4

#### Load info of the receiver question which you shared `/question/load_info_receiver`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int` (id of user who create question!)
  - `id_question`: `int`
  >Sample: http://localhost:8080/question/load_info_receiver?id_user=3&id_question=18

#### Load List Questions which you don't shared for other `/question/list_question_you_no_shared`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int` (id of user who create question!)
  >Sample: http://localhost:8080/question/list_question_you_no_shared?id_user=1

#### Load List Questions which you be shared from other `/question/list_question_be_shared_for_you`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int` (id of user who receive create question!)
  >Sample: http://localhost:8080/question/list_question_be_shared_for_you?id_user=2

#### Load list user that you can share question for them `/question/list_user_you_can_share`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int` (id of user who create question!)
  - `id_question`: `int`
  >Sample: http://localhost:8080/question/list_user_you_can_share?id_user=1&id_question=18

#### Share question of you for the others `/question/share`
- Method: `POST`
- Parameters: **(json)**
  - `id_question`: `int`
  - `id_owned`: `int`
  - `id_users`: `Array<int>`

#### Remove share question of you for the others `/question/remove_share`
- Method: `POST`
- Parameters: **(json)**
  - `id_question`: `int`
  - `id_owned`: `int`
  - `id_users`: `Array<int>`

### Topic
#### Create Topic `/topic/create`
- Method: `POST`
- Parameter: **(json)**
  - `id_parent`: `int`
  - `id_teacher`: `int`
  - `text`: `string` (content of the topic)

  Sample:
  - Without the parent topic:
  ```javascript
  {
    "id_parent": "",
    "id_teacher": "2",
    "text": "Homework"
  }
  ```
  - With the parent topic:
  ```javascript
  {
    "id_parent": "1",
    "id_teacher": "1",
    "text": "Homework"
  }
  ```
#### Rename Topic `/topic/rename`
- Method: `POST`
- Parameter: **(json)**
  - `id`: `int`
  - `name_topic`: `string`

#### Load list test in Topic `/topic/test`
- Method: `POST`
- Parameter: **(json)**
  - `id`: `int` (idTopic)
  - `idUser`: `int`

#### Load list test not in Topic `/topic/test_not_in`
- Method: `POST`
- Parameter: **(json)**
  - `id`: `int` (idTopic)
  - `idUser`: `int`

#### Modify tests for topic `/topic/modify_test`
- Method: `POST`
- Parameters: **(json)**
  - `id_topic`: `int`
  - `list_id_test`: `[int]`
  - `option`: `boolean` (false: remove, true: add)

  Sample:
  ```javascript
  {
    "id_topic": "1",
    "list_id_test": [
      "1",
      "2",
      "3",
      "4"
    ],
    "option": true
  }
  ```

#### Insert Subtopic `/topic/insert_topic`
- Method: `POST`
- Parameter: **(json)**
  - `id_parent`: `int`
  - `id_child`: `int`

  Sample:
  ```javascript
  {
    "id_parent": "1",
    "id_child": "2"
  }
  ```

#### Delete Topic `/topic/delete`
- Method: `POST`
- Parameter: **(json)**
  - `id_topic`: `int`
  - `id_teacher`: `int`

  Sample:
  ```javascript
  {
    "id_topic": "1",
    "id_teacher": "12"
  }
  ```
#### Load List Topic of User `/topic/user`
- Method: `POST`
- Parameter: **(json)**
  - `id`: `int`

### Test
#### Get test detail
- Method: `GET`
- Parameter:
  - `id`: `int`

#### Create test `/test/create`
- Method: `POST`
- Parameters: **(json)**
  - `id_teacher`: `int`
  - `title`: `string`
  <!-- - `total_point`: `int` -->
  - `duration`: `int` (second)
  - `is_private`: `bool`
  - `is_realtime`: `bool`
  - `date_start`: `datetime`
  - `date_end`: `datetime` (optional, required when normal test)
  - `url_image`: `string` (image url, optional, can NULL)
  - `allowed_number_do`: `int` (optional, required when normal test) '0: infinity, n: n times'
  - `mode_point` : `int` (0: highest, 1: average, 2: most recent only need for normal test, for realtime by default is most recent)  

#### Create test `/test/update`
- Method: `POST`
- Parameters: **(json)**
  <!-- - `id_teacher`: `int` -->
  - `id`: `int`
  - `title`: `string`
  <!-- - `total_point`: `int` -->
  - `duration`: `int` (second) (optional)
  <!-- - `is_private`: `bool`
  - `is_realtime`: `bool` -->
  <!-- - `date_start`: `datetime` -->
  - `date_end`: `datetime` (optional, required when normal test) (Ui need check greater than date_start)
  - `url_image`: `string` (image url, can NULL)
  - `allowed_number_do`: `int` (optional, required when normal test) '0: infinity, n: n times'
  <!-- - `mode_point` : `int` (0: highest, 1: average, 2: most recent only need for normal test, for realtime by default is most recent)   -->

#### Load list question of the test `/test/question_in`
 Method: `GET`
- Parameters: **(url)**
  - `id_test`: `int`
  - `id_user`: `int` (posses the test)
> Example: http://localhost:8080/test/question_in?id_test=1&id_user=1

#### Load list question not in the test of user or shared by other user `/test/question_not_in`
 Method: `GET`
- Parameters: **(url)**
  - `id_test`: `int`
  - `id_user`: `int` (posses the test)
> Example: http://localhost:8080/test/question_not_in?id_test=1&id_user=1


#### Add question or config info question for the test `/test/add_update_question`
- Method: `POST`
- Parameters: **(json)**
  - `id_test`: `int`
  - `id_questions`: `Array<infoQuestion>`
- Structure `infoQuestion`:
  - `id_question`: `int`
  - `time_limit`: `string`
  - `point`: `int`

#### Delete test `/test/delete`
- Method: `POST`
- Parameters: **(json)**
  - `id_test`: `int`
  - `id_teacher`: `int`

#### Insert Test to Topic or delete from Topic (follow UI) `/test/modify_to_topic`
- Method: `POST`
- Parameters: **(json)**
  - `id_test`: `int`
  - `list_id_topic`: `[int]`
  - `option`: `boolean` (false: remove, true: add)

#### Insert Test to Class or delete from Class (follow UI) `/test/modify_to_class`
- Method: `POST`
- Parameters: **(json)**
  - `id_test`: `int`
  - `list_id_class`: `[int]`
  - `option`: `boolean`

#### Load statistical information about point Test_Did of user `/test/statistic`
- Method: `GET`
- Parameter: **url**
  - `id`: `int` (idTest)
  >Sample: http://localhost:8080/test/statistic?id=8
  <br/> structure of returning value:
  <br/>- pointTest: point of the Test <br /> - avgAllUser: Average point of all user do the test <br/> - classifyPointUser: classifying point of user on point of the Test (A (75-100%), B(50-75%), C(25-50%) and D(0-25%)) <br/> - rateTrueQuestion: right ratio of user for per a Question <br/> - finalPointUser: final point for the test of per user

#### Load statistical information about point Test_Did of user follow per Class `/test/statistic_per_class`
 Method: `POST`
- Parameters: **(json)**
  - `id_test`: `int`
  - `id_class`: `int` (null if it is public)
  ><br/> structure of returning value:
  <br/>- pointTest: point of the Test <br /> - avgAllUser: Average point of all user do the test in per class <br/> - classifyPointUser: classifying point of user on point of the Test in per class (A (75-100%), B(50-75%), C(25-50%) and D(0-25%)) <br/> - rateTrueQuestion: right ratio of user for per a Question in per class <br/> - finalPointUser: final point for the test of per user in per class

#### Load List Class of user that the test is contained (follow UI) `/test/in_classes`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int`
  - `id_test`: `int`
  >Sample: http://localhost:8080/test/in_classes?id_user=2&id_test=9

#### Load List Class of user that the test isn't in (follow UI) `/test/not_in_classes`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int`
  - `id_test`: `int`
  >Sample: http://localhost:8080/test/not_in_classes?id_user=2&id_test=9

#### Load List Topic of user that the test is contained (follow UI) `/test/in_topics`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int`
  - `id_test`: `int`
  >Sample: http://localhost:8080/test/in_topics?id_user=1&id_test=1

#### Load List Topic of user that the test isn't in (follow UI) `/test/not_in_topics`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int`
  - `id_test`: `int`
  >Sample: http://localhost:8080/test/not_in_topics?id_user=1&id_test=1

#### Load List Tests of creator  (follow UI) `/test/list_test_of_creator`
- Method: `GET`
- Parameter: **url**
  - `id_user`: `int`
  >Sample: http://localhost:8080/test/list_test_of_creator?id_user=1

#### Load detail The Test for creator  (follow UI) `/test/detail_for_creator`
- Method: `GET`
- Parameter: **url**
  - `id_test`: `int`
  >Sample: http://localhost:8080/test/detail_for_creator?id_test=7

#### Submit
#### Submit Test_Did `/submit/`
- Method: `POST`
- Parameter: **(json)**
  - `id_student`: `int`
  - `id_test`: `int`
  - `id_class`: `int` (it is null if public of the test )
  - `do_test`: `Array` (Contains user answers by question id) <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`id`:`int`, <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`answer`: `Array<string>` (Ex: ["1","2","3"] \\\\(chooseAnswer) or (ArrangeAnswer with orderly) with "1", "2" ,"3" is idAnswer) <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},... <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;] <br />
  - `start_time`: `int` (Date.now(): milliseconds)
  <!-- - `is_submitted`: `boolean` (If true will score, if false only save!) -->
  Sample:
  ```javascript
  {
    "id_student": 1,
    "id_test": 2,
    "do_test":[
                {
                  "id":9,
                  "answer":["1","2","3"] //choose (1,2,3: id answer)
                },
                {
                  "id":10,
                  "answer":["45-44","46-47","51-50"] //match (45,44,46,...: id answer)
                },
                {
                  "id":5,
                  "answer":["16","18","19"] //arrange (16,18,19: id answer orderly!)
                },
                {
                  "id":12,
                  "answer": ["e:,m","la","#gia"] //fill (answer is string, cannot contain the characters "\000", "\001", "\002")
                }
      ],
      "start_time":"1585306831000",
  }
  ```

#### Submit Test_Did for Guest`/submit/guest_submit`
- Method: `POST`
- Parameter: **(json)**
  - `id_test`: `int`
  - `do_test`: `Array` (Contains user answers by question id) <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{ <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`id`:`int`, <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`answer`: `Array<string>` (Ex: ["1","2","3"] \\\\(chooseAnswer) or (ArrangeAnswer with orderly) with "1", "2" ,"3" is idAnswer) <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},... <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;] <br />
  - `start_time`: `int` (Date.now(): milliseconds)
  <!-- - `is_submitted`: `boolean` (If true will score, if false only save!) -->
> similar to api / submit but return parameters will different
#### Get point of Test_Did `/submit/get_point/`
- Method: `GET`
- Parameter:
  - `id`: `int`

#### Get list final point for test of user follow per class `/submit/get_list_final_point/`
- Method: `GET`
- Parameter:
  - `id_user`: `int`
  > Example: http://localhost:8080/submit/get_list_final_point?id_user=1
#### Get list Test_Did follow user and test `/get_test_did/`
- Method: `GET`
- Parameter:
  - `id_user`: `int`
  - `id_test`: `int`


### Evaluate
#### Load list evaluations about the Test follow User `/evaluate/load_test_user` 
- Method: `GET`
- Parameter: **(url)**
  - `id_test`: `int`
  - `id_user`: `int` ( <= 0 if it is Guest)
  > http://localhost:8080/evaluate/load_test_user/?id_test=1&id_user=10

#### Evaluate the test `/evaluate/test` (only for User, Guest can't use it)
- Method: `POST`
- Parameters: **(json)**
  - `id_test`: `int`
  - `id_user`: `int`
  - `comment`: `string`
  - `star`: `int` (in range (1 -> 5))

#### Delete the evaluation of User for the test `/evaluate/delete_test` (only for User, Guest can't use it)
- Method: `POST`
- Parameters: **(json)**
  - `id_test`: `int`
  - `id_user`: `int`